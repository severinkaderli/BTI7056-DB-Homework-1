---
title: "BTI7056-DB"
subtitle: "Homework 1"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
...

# Aufgabe 1
> Sie möchten in einer Tabelle in einer Datenbank speichern, an welchem Datum Sie
> sich mit welcher Person an welchem Ort getroffen haben. 
> 
> 1. Geben Sie das Schema dieser Datenbank.
> 2. Geben Sie ein Beispiel fur eine Instanz dieses Schemas.

#### 1.
`meetings(id, timestamp, person, location)`

#### 2.

| id  | timestamp  | person       | location |
| --- | ---------- | ------------ | -------- |
| 1   | 1234567889 | Marius Schär | BFH      |
| 2   | 1550851935 | Barack Obama | Hawaii   |
| 3   | 178851935  | Dalai Lama   | Lhasa    |
| 4   | 2345567889 | Marius Schär | Thun     |

# Aufgabe 2
> 1. Erweitern Sie das Beispiel aus der letzten Aufgabe: Sie wollen in der selben Tabelle auch noch die Telefonnummer der Personen speichern.
> 2. Erklären Sie anhand dieses Beispiels Redundanz in einer Datenbank.
> 3. Erklären Sie, wie aus dieser Redundanz Inkonsistenz entstehen kann.

#### 1.
`meetings(id, timestamp, person, location, telnum)`

#### 2.

| id  | timestamp  | person       | location | telnumber    |
| --- | ---------- | ------------ | -------- | ------------ |
| 1   | 1234567889 | Marius Schär | BFH      | 062 550 2771 |
| 2   | 1550851935 | Barack Obama | Hawaii   | 020 916 7030 |
| 3   | 178851935  | Dalai Lama   | Lhasa    | 078 343 3434 |
| 4   | 2345567889 | Marius Schär | Thun     | 334 346 2555 |

#### 3.
Wenn eine Person eine andere Telefonnummer hat, muss die Telefonnummer in jedem
Record angepasst werden. Das ist langsam und kann potentiell vergessen werden.

# Aufgabe 3
> Gegeben seien die Tabellen `instructor` und `department` als CSV-Dateien.
> Implementieren Sie folgende Abfragen in Pseudocode oder einer Ihnen genehmen
> Programmiersprache: 
> ```SQL
> 1.
> SELECT namefrom instructor WHERE ID = '22222'
>
> 2.
> SELECT buildingfrom instructor, department WHERE instructor.dept_name = department.dept_name andname = 'Einstein'
> ```

```{.java .numberLines}
import java.util.ArrayList;
import java.util.List; 
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
        checkResult(findNameOfInstructorById("22222"), "Einstein");
        checkResult(findBuildingOfInstructor("Einstein"), "Watson");
    }

    private static void checkResult(String actual, String expected){
        if(actual == null || !actual.equals(expected)){
            throw new IllegalArgumentException(
              String.format("Expected \"%s\", but got \"%s\"",
              expected, actual));
        }
        System.out.printf("The result \"%s\" is correct!%n", actual);
    }

    /**
     * Runs a query like:
     * select name
     * from instructor
     * where ID = '22222'
     */
    private static String findNameOfInstructorById(String id){
        // iterate all instructors
        String resultRow =  getInstructorCsv().stream() 
            // only keep the row with the matching ID
            .filter(row -> getField(row, 0).equals(id)) 
            .findFirst().orElseThrow(NoSuchElementException::new);
        // extract the instructor name
        return getField(resultRow, 1); 
    }

    /**
     * Runs a query like:
     * select building
     * from instructor, department
     * where instructor.dept_name = department.dept_name and
     * name = 'Einstein'
     */
    private static String findBuildingOfInstructor(String name){
        // iterate all instructors
        String instructorRow =  getInstructorCsv().stream() 
                // only keep the row with the matching name
                .filter(row -> getField(row, 1).equals(name)) 
                .findFirst().orElseThrow(NoSuchElementException::new);
        // extract the instructors department
        String department = getField(instructorRow, 2); 

        // iterate all departments
        String resultRow  =  getDepartmentCsv().stream() 
                // only keep the row with the matching department
                .filter(row -> getField(row, 0).equals(department)) 
                .findFirst().orElseThrow(NoSuchElementException::new);
        // extract the departments building
        return getField(resultRow, 1); 
    }


    /**
     * Splits row by ',' and returns the field-th field. Zero indexed.
     */
    private static String getField(String row, int field){
        return row.split(",")[field];
    }

    private static List<String> getInstructorCsv(){
        List<String> lines = new ArrayList<>(3);
        lines.add("22222,Einstein,Physics,95000");
        lines.add("12121,Wu,Finance,90000");
        lines.add("32343,El Said,History,60000");
        return lines;
    }

    private static List<String> getDepartmentCsv(){
        List<String> lines = new ArrayList<>(3);
        lines.add("Physics,Watson,70000");
        lines.add("Finance,Painter,120000");
        lines.add("History,Painter,50000");
        return lines;
    }
}
```

> Warum nutzt man eine deklarative Sprache wie SQL zur Datenbankabfrage? Warum
> nicht eine prozedurale Programmiersprache wie Java oder Python?

Die Abfragen in SQL sind für Menschen **einfacher** zu verstehen,
**kürzer** und **schöner**.
Durch die Optimierungen, welche von Datenbanken anhand der gespeicherten Daten,
sowie mit besseren Datenstrukturen durchgeführt werden können,
wird **keine Performanceeinbusse, gar ein Performancegewinn** gemacht.

# Aufgabe 4
> Gegeben sei ein Array in der globalen Variable `a` mit Kontoständen in
> nicht-flüchtigem Speicher. Es soll die Integritätsbedingung gelten, dass die
> Summe aller Werte des Arrays gleich bleibt. Nun soll das erste Konto geleert und
> auf das zweite Konto überwiesen werden. Dazu gibt es die Funktion:
> ```{.java .numberLines}
> void transact(int[] a) { 
>   a[1] = a[1] + a[0];
>   a[0] = 0;
> }
> ```

Wenn das System nach der *Zeile 2* abstürzt, wird der Kontostand 0 nie angepasst.
Somit steigt der Gesamtbetrag, was die Integritätsbedingung verletzt.

> Erklären Sie, wie bei einem Systemabsturz die Integritätsbedingung
> verletzt werden kann. Erweitern Sie die Funktion auf geeignete Weise, um
> sicherzustellen, dass die Integritätsbedingung zu jedem Zeitpunkt gilt.

Durch ein *copy-on-write*-Pattern, können wir am Ende auf *Zeile 4* in einer
einzigen Aktion den persistenten Speicher verändern. Falls diese Instruktion
fehlschlägt, wird der Speicher nicht verändert.
Falls sie erfolgreich ausgeführt wird, ist sichergestellt dass die Berechnung
korrekt ist.

```{.java .numberLines}
void transact(int[] a) { 
  int[] b = Arrays.copyOf(a, a.length);
  b[1] = b[1] + b[0];b[0] = 0;
  a = b;
}
```

